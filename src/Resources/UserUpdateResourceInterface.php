<?php

namespace Scito\Keycloak\Admin\Resources;

interface UserUpdateResourceInterface
{
    public function username(?string $username): UserUpdateResourceInterface;

    public function email(?string $email): UserUpdateResourceInterface;

    public function enabled(?bool $enabled): UserUpdateResourceInterface;

    public function firstName(?string $firstName): UserUpdateResourceInterface;

    public function lastName(?string $lastName): UserUpdateResourceInterface;

    public function password(?string $password): UserUpdateResourceInterface;

    public function temporaryPassword(?string $password): UserUpdateResourceInterface;

    public function save(): void;
}
